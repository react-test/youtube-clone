import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import YTSearch from 'youtube-api-search';
import VideoDetail from './components/video_detail';

import _ from 'lodash';

const API_KEY = "AIzaSyBqt87vjiLAPvdjc_lnL0W7pQ73rPAn96A";


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            videos: [],
            selectVideo: ''
        };


        this.onSearchText('dear gods');
    }

    onSearchText(term) {
        console.log(term);
        YTSearch({ key: API_KEY, term }, (videos) => {
            this.setState({ videos, selectVideo: videos[0] });
        });
    }

    render() {
        const searchText = _.debounce(term => this.onSearchText(term), 300);
        return (
            <div>
                <SearchBar onSearchChange={searchText} />
                <VideoDetail video={this.state.selectVideo} />
                <VideoList videos={this.state.videos} onVideoSelect={selectVideo => this.setState({ selectVideo })} />

            </div>

        );
    }
}

ReactDOM.render(<App />, document.querySelector('.container'));
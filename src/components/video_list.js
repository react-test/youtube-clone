import React from 'react'
import VideoItem from './video_list_item';

const VideoList = (props) => {


    const videoItems = props.videos.map((video) => {
        return (
            <VideoItem video={video} key={video.etag} onVideoSelect={props.onVideoSelect}/>
        );
    })

    return (
        <ul className="col-md-4 list-group">
            {videoItems}
        </ul>
    );
};

export default VideoList;